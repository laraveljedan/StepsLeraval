# vue cod_eho_lic food
.editorconfig  
![](images/2023-06-07-09-58-22.png)


install vite
![](images/2023-06-07-09-07-53.png)

run npm install & dev  
![](images/2023-06-07-09-09-29.png)

package.json change dev  
![](images/2023-06-07-09-12-00.png)

preview ff  
![](images/2023-06-07-09-12-38.png)

clean boilerplate - template->script->style  
![](images/2023-06-07-09-26-19.png)

# install tw with vite  
![](images/2023-06-07-09-27-19.png)

vec imamo vue instaliran  
![](images/2023-06-07-09-30-01.png)

install tw  
![](images/2023-06-07-09-30-16.png)

config tw  
![](images/2023-06-07-09-30-45.png)

add directives to css file  
![](images/2023-06-07-09-31-06.png)

# install vue router v4  
![](images/2023-06-07-09-49-25.png)

create file src/router/index.js  
![](images/2023-06-07-09-50-32.png)

![](images/2023-06-07-09-57-33.png)
![](images/2023-06-07-09-57-54.png)

add router to main  
![](images/2023-06-07-09-59-12.png)

# home component
create src/views/Home.vue  
![](images/2023-06-07-10-00-34.png)
![](images/2023-06-07-10-01-25.png)

import Home component in router/index.js  
![](images/2023-06-07-10-01-58.png)

add <router-view /> in App.vue  
![](images/2023-06-07-10-02-36.png)

# install vuex  
![](images/2023-06-07-10-52-41.png)

start npm run dev again  
![](images/2023-06-07-10-53-11.png)

create src/store/index.js  
![](images/2023-06-07-10-53-56.png)

config store  
![](images/2023-06-07-11-32-52.png)

create actions.js, mutations.js, state.js, getters.js  
![](images/2023-06-07-11-33-29.png)

![](images/2023-06-07-11-33-54.png)

import store functions  
![](images/2023-06-07-12-34-30.png)
![](images/2023-06-07-12-34-52.png)

config store in main.js  
![](images/2023-06-07-12-35-21.png)

add variable in state.js  
![](images/2023-06-07-12-37-26.png)

call variable in Home component  
![](images/2023-06-07-12-38-03.png)

import * as je bolje od import za funkcije  
![](images/2023-06-07-12-41-38.png)

# input forms store  
install tailwind css forms  
![](images/2023-06-07-12-53-24.png)

input form Home compenent  
![](images/2023-06-07-13-17-18.png)

some variables from store Home component  
![](images/2023-06-07-13-17-39.png)

template output Home   
![](images/2023-06-07-13-18-10.png)

use single quotes  
![](images/2023-06-07-13-18-35.png)

make letters an array using split  
![](images/2023-06-07-13-23-08.png)

print letters  
![](images/2023-06-07-13-23-35.png)

set router link to="/"  
![](images/2023-06-07-13-24-01.png)

# install axios  
![](images/2023-06-07-13-42-16.png)

touch src/axiosClient.js  
![](images/2023-06-07-13-43-11.png)

axios example  
![](images/2023-06-07-13-43-35.png)

meal api base endpoint  
![](images/2023-06-07-13-44-10.png)

base api  
![](images/2023-06-07-13-44-46.png)

export axiosClient  and import axios lib  
![](images/2023-06-07-14-38-36.png)
![](images/2023-06-07-14-35-23.png)

Home component onMounted hook  
![](images/2023-06-07-14-36-18.png)

list endpoints all ingredients  
![](images/2023-06-07-14-36-41.png)