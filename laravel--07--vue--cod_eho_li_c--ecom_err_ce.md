# vue cod_eho_li_c ecom_err_ce
install lara  
![](images/2023-06-08-09-11-45.png)
![](images/2023-06-08-09-12-19.png)

migrate  
![](images/2023-06-08-09-13-03.png)
serve  
![](images/2023-06-08-09-13-29.png)

database  
![](images/2023-06-08-09-13-48.png)

# database models  
model Product  
![](images/2023-06-08-09-16-24.png)
model Order  
![](images/2023-06-08-09-16-56.png)
model Country  
![](images/2023-06-08-09-44-42.png)
model CartItem  
![](images/2023-06-08-09-45-58.png)
model OrderDetail  
![](images/2023-06-08-09-46-12.png)
model OrderItem  
![](images/2023-06-08-09-46-30.png)
model Customer  
![](images/2023-06-08-09-46-51.png)
model CustomerAddress  
![](images/2023-06-08-09-47-06.png)

# models  
Products table  
![](images/2023-06-08-09-48-04.png)
foreign id Products  
![](images/2023-06-08-09-48-30.png)

Orders table  
![](images/2023-06-08-10-08-49.png)

Countries table  
![](images/2023-06-08-10-09-49.png)
![](images/2023-06-08-10-10-01.png)

Cart-items table  
![](images/2023-06-08-11-37-48.png)
![](images/2023-06-08-11-37-54.png)

Order-details table  
![](images/2023-06-08-11-38-35.png)
![](images/2023-06-08-11-38-43.png)

Order-items table  
![](images/2023-06-08-11-57-52.png)
![](images/2023-06-08-11-58-00.png)

Create-payments table  
![](images/2023-06-08-12-38-31.png)
![](images/2023-06-08-12-38-39.png)

Create-customers table  
![](images/2023-06-08-22-21-49.png)
![](images/2023-06-08-22-22-27.png)

Create-customer-addresses table  
![](images/2023-06-08-22-22-52.png)
![](images/2023-06-08-22-23-11.png)

migrate db  
![](images/2023-06-08-22-23-29.png)