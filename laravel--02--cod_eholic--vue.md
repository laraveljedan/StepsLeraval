# codeholic vue
✅install lara 
![](images/2023-04-24-12-43-15.png)

✅lara version 
![](images/2023-04-24-12-44-09.png)

✅migrate db  
![](images/2023-04-24-12-46-03.png)

✅vite init  
![](images/2023-04-24-12-48-51.png)

✅vite init vue  
![](images/2023-04-24-12-49-17.png)

✅vue install vuex  
![](images/2023-04-24-12-50-01.png)

✅vue run different port  
![](images/2023-05-04-20-22-34.png)

✅vue store  
![](images/2023-04-24-12-52-21.png)

✅vue mount store  
![](images/2023-04-24-13-04-31.png)

✅App.vue mapState  
![](images/2023-04-24-13-05-21.png)

✅TailWind install npm  
![](images/2023-04-24-13-06-39.png)
![](images/2023-04-24-13-05-54.png)
![](images/2023-04-24-13-06-18.png)
![](images/2023-04-24-13-06-49.png)
![](images/2023-04-24-13-07-13.png)

✅install heroicons and forms  
![](images/2023-04-24-13-09-12.png)

✅install vue router  
![](images/2023-04-24-13-10-02.png)

✅config vue router  
![](images/2023-04-24-13-13-48.png)

✅main.js use(router)  
![](images/2023-04-24-13-18-57.png)

✅router/index.js path /login  
![](images/2023-04-24-13-20-03.png)

✅router/index.js path /, /login, /register  
![](images/2023-04-24-13-20-41.png)

✅import router/index.js  
![](images/2023-04-24-13-22-57.png)

✅vue view Dashboard.vue  
![](images/2023-04-24-13-21-29.png)

✅vue view Dashboard, Login, Register  
![](images/2023-04-24-13-21-57.png)

✅STOP Vue => 28:00  
![](images/2023-04-24-13-36-58.png)

=> 51:00 START  Lara  
✅commit: generate models and migrations  
![](images/2023-04-24-13-53-52.png)

✅make:model Survey  
![](images/2023-04-24-13-42-38.png)

✅make:model SurveyQuestion  
![](images/2023-04-24-13-44-15.png)

✅make:model SurveyAnswer  
![](images/2023-04-24-13-44-51.png)

✅make:model SurveyQuestionAnswer  
![](images/2023-04-24-13-45-07.png)

✅create_surveys_table  
![](images/2023-04-24-13-51-27.png)

✅create_survey_questions_table  
![](images/2023-04-24-13-52-10.png)

✅create_survey_answers_table  
![](images/2023-04-24-14-43-37.png)

✅create_survey_question_answers_table  
![](images/2023-04-24-14-44-13.png)

AuthController  
![](images/2023-04-24-14-46-31.png)

api.php set route  
![](images/2023-04-24-14-48-32.png)
![](images/2023-04-24-14-48-43.png)

api.php set login route  
![](images/2023-04-24-15-10-24.png)