# laravel ang Learn_ningPo_ints
php version  
![](images/2023-06-20-19-19-24.png)

angular version  
![](images/2023-06-20-19-22-22.png)

lara conf 8.6  
![](images/2023-06-20-19-14-16.png)

ang /frontend   
![](images/2023-06-20-19-15-40.png)

start server /backend   
![](images/2023-06-20-19-16-12.png)

start server /frontend  
![](images/2023-06-20-19-18-53.png)

jwt-auth  
![](images/2023-06-20-19-23-29.png)

![](images/2023-06-20-19-24-23.png)

![](images/2023-06-20-19-24-59.png)

add bootstrap to ng  
![](images/2023-06-20-19-25-37.png)

![](images/2023-06-20-19-26-03.png)

config angular.json  
![](images/2023-06-20-19-36-38.png)

config ng-bootstrap  
![](images/2023-06-20-19-37-52.png)

Navbar bootstrap  
![](images/2023-06-20-19-38-23.png)

generate component navbar  
![](images/2023-06-20-19-38-51.png)

generate component login, signup  
![](images/2023-06-20-19-43-27.png)

app-routing ng  
![](images/2023-06-20-19-44-14.png)

lara generate JWTAuth  
![](images/2023-06-20-19-54-13.png)

lara generate jwt:secret  
![](images/2023-06-20-19-55-03.png)

.env JWT  
![](images/2023-06-20-19-55-21.png)