# lara 01
install lara globaly \
✅![](images/2023-04-23-02-02-38.png)

✅ install lara project \
![](images/2023-04-23-02-03-00.png)

✅migrate db - yes \
![](images/2023-04-23-02-04-13.png)

✅ php artisan serve  
![](images/2023-04-23-02-04-41.png)

✅ vite install react \
![](images/2023-04-23-02-05-27.png)

✅ vite install \
![](images/2023-04-23-02-06-01.png)

✅ vite port 3000 \
![](images/2023-04-23-02-06-51.png)

✅ router.jsx \
![](images/2023-04-23-02-20-49.png)

✅ router.jsx \
![](images/2023-04-23-02-21-36.png)

✅ main.jsx provider \
![](images/2023-04-23-02-22-08.png)

✅ router.jsx notFound \
![](images/2023-04-23-02-22-50.png)

✅ components guest, default layout \
![](images/2023-04-23-02-24-07.png)

✅ router.jsx default layout children \
![](images/2023-04-23-02-25-20.png)

✅ views \
![](images/2023-04-23-02-25-46.png)

✅ Guest Layout Outlet import \
![](images/2023-04-23-02-26-18.png)

✅ Default Layout Outplet \
![](images/2023-04-23-02-55-29.png)

✅ router.jsx default layout END 32:59 \
![](images/2023-04-23-02-55-53.png)

=> 32:59 BREAK \ 

![](images/2023-04-24-01-33-26.png)


1:17:00 => START 

lara controller create in Api dir \
![](images/2023-04-24-01-32-57.png)

create 3 functions, login, signup, logout \
![](images/2023-04-24-01-35-48.png)

generate Login request \
![](images/2023-04-24-01-36-41.png)

generate Signup request \
![](images/2023-04-24-01-37-08.png)

added LoginRequest, SignupRequest, Request arguments \
![](images/2023-04-24-01-37-59.png)

in api.php add route for /signup \
![](images/2023-04-24-01-39-10.png)
![](images/2023-04-24-01-38-55.png)

in api.php add all three methods as routes \
![](images/2023-04-24-01-39-55.png)